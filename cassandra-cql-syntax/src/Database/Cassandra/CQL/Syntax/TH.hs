module Database.Cassandra.CQL.Syntax.TH (cql) where

import Database.Cassandra.CQL.Syntax.Parser (parseCQL)
import Language.Haskell.TH.Quote

import qualified Language.Haskell.TH as TH

cql :: QuasiQuoter
cql = QuasiQuoter
    { quoteExp = quoteCqlExp
    , quotePat = undefined
    , quoteDec = undefined
    , quoteType = undefined
    }

quoteCqlExp :: String -> TH.Q TH.Exp
quoteCqlExp s = do
    pos    <- getPosition
    cqlExp <- case parseCQL pos s of
        Left err -> fail (show $ err)
        Right () -> return s
    dataToExpQ (const Nothing) cqlExp

getPosition :: TH.Q (String, Int, Int)
getPosition = fmap transPos TH.location where
   transPos loc = (TH.loc_filename loc,
                   fst (TH.loc_start loc),
                   snd (TH.loc_start loc))
