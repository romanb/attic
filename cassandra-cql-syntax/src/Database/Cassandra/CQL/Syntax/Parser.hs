module Database.Cassandra.CQL.Syntax.Parser (parseCQL) where

-- Parser for the Cassandra Query Language (v3)
-- Based on Cassandra version 1.2.7
-- https://github.com/apache/cassandra/blob/cassandra-1.2.7/src/java/org/apache/cassandra/cql3/Cql.g
-------------------------------------------------------------------------------

import Control.Applicative ((<*), (*>), pure)
import Text.Parsec
import Text.Parsec.Language
import qualified Text.Parsec.Token as T

langDef :: LanguageDef ()
langDef = emptyDef
    { T.commentStart = "/*"
    , T.commentEnd = "*/"
    , T.commentLine = "--"
    , T.identStart = letter
    , T.identLetter = alphaNum <|> char '_'
    , T.caseSensitive = False
    , T.reservedNames = ["ADD", "ALTER", "AND", "ANY", "APPLY", "ASC",
                         "AUTHORIZE", "BATCH", "BEGIN", "BY", "COLUMNFAMILY",
                         "CREATE", "DELETE", "DESC", "DROP", "EACH_QUOROM",
                         "FROM", "GRANT", "IN", "INDEX", "INSERT", "INTO",
                         "KEYSPACE", "LIMIT", "LOCAL_QUORUM", "MODIFY", "NORECURSIVE",
                         "OF", "ON", "ONE", "ORDER", "PRIMARY", "QUOROM", "REVOKE",
                         "SCHEMA", "SELECT", "SET", "TABLE", "THREE", "TOKEN",
                         "TRUNCATE", "TWO", "UPDATE", "USE", "USING", "WHERE",
                         "WITH"]
    }

tp :: T.TokenParser ()
tp = T.makeTokenParser langDef

type Parser = Parsec String () ()

parseCQL :: (String, Int, Int) -> String -> Either ParseError ()
parseCQL (file, line, col) = parse (updatePosition file line col >> cqlStmts) ""

updatePosition :: String -> Int -> Int -> Parser
updatePosition file line col = do
    pos <- getPosition
    setPosition $
        (flip setSourceName) file $
        (flip setSourceLine) line $
        (flip setSourceColumn) col $
        pos

cqlStmts :: Parser
cqlStmts = whiteSpace >> cqlStmt *> optional semi *> many (cqlStmt *> semi) *> eof

cqlStmt :: Parser
cqlStmt = (  useStatement
         <|> selectStatement
         <|> insertStatement
         <|> updateStatement
         <|> deleteStatement
         <|> batchStatement
         <|> try alterKeyspaceStatement
         <|> alterTableStatement
         <|> try dropKeyspaceStatement
         <|> try dropTableStatement
         <|> dropIndexStatement
         <|> try createKeyspaceStatement
         <|> try createTableStatement
         <|> createIndexStatement
         <|> truncateStatement
          )

-- Basic parsers & combinators
-------------------------------------------------------------------------------

identifier :: Parser
identifier = T.identifier tp *> pure ()

parens :: Parsec String () a -> Parser
parens = (*> pure ()) . T.parens tp

angles :: Parsec String () a -> Parser
angles = (*> pure ()) . T.angles tp

reservedOp :: String -> Parser
reservedOp = T.reservedOp tp

reserved :: String -> Parser
reserved = T.reserved tp

semi :: Parser
semi = T.semi tp *> pure ()

symbol :: String -> Parser
symbol = (*> pure ()) . T.symbol tp

lexeme :: Parsec String () a -> Parser
lexeme = (*> pure ()) . T.lexeme tp

commaSep :: Parsec String () a -> Parser
commaSep = (*> pure ()) . T.commaSep tp

commaSep1 :: Parsec String () a -> Parser
commaSep1 = (*> pure ()) . T.commaSep1 tp

braces :: Parsec String () a -> Parser
braces = (*> pure ()) . T.braces tp

brackets :: Parsec String () a -> Parser
brackets = (*> pure ()) . T.brackets tp

colon :: Parser
colon = T.colon tp *> pure ()

comma :: Parser
comma = T.comma tp *> pure ()

whiteSpace :: Parser
whiteSpace = T.whiteSpace tp

tableName :: Parser
tableName = optional (try $ ident *> char '.') *> ident

integer :: Parser
integer = optional (reservedOp "-") *> lexeme (skipMany1 digit)

ident :: Parser
ident = identifier <|> quotedIdent

quotedIdent :: Parser
quotedIdent = lexeme $ between (char '"') (char '"') $
    skipMany1 $ try $ (string "\"\"" *> pure '"') <|> anyChar

keyword :: String -> Parser
keyword = reserved

stringLiteral :: Parser
stringLiteral = between (char '\'') (char '\'') $
    skipMany1 $ try $ (string "''" *> pure '\'') <|> anyChar

float :: Parser
float = lexeme $
    optional (symbol "-") *>
    skipMany1 digit *>
    optional (char '.' *> optional (skipMany1 digit)) *>
    optional (oneOf ['e','E'] *> optional (oneOf ['+','-']) *> skipMany1 digit)

uuid :: Parser
uuid = hexs 8 *> hexs 4 *> hexs 4 *> hexs 4 *> hex 12
  where
    sep    = char '-'
    hex  n = count n hexDigit *> pure ()
    hexs n = hex n *> sep

boolean :: Parser
boolean = keyword "true" <|> keyword "false"

number :: Parser
number = try float <|> integer

constant :: Parser
constant = lexeme $  stringLiteral
                 <|> try uuid
                 <|> try blob
                 <|> number
                 <|> boolean

blob :: Parser
blob = char '0' *> oneOf ['x','X'] *> skipMany1 hexDigit

variable :: Parser
variable = symbol "?"

term :: Parser
term =  value
    <|> functionName *> parens (commaSep term)
    <|> parens columnType *> term -- "type casts"

value :: Parser
value = variable <|> constant <|> collectionLiteral <|> reserved "NULL"

functionName :: Parser
functionName = ident <|> keyword "TOKEN"

collectionLiteral :: Parser
collectionLiteral =  try mapLiteral
                 <|> try (symbol "{" *> symbol "}")
                 <|> setLiteral
                 <|> listLiteral

mapLiteral :: Parser
mapLiteral = braces $ commaSep1 (term *> colon *> term)

setLiteral :: Parser
setLiteral = braces $ commaSep1 term

listLiteral :: Parser
listLiteral = brackets $ commaSep1 term

properties :: Parser
properties = sepBy1 property (reserved "AND") *> pure ()

property :: Parser
property = ident *> reservedOp "=" *> (ident <|> constant <|> mapLiteral)


-- DML
-------------------------------------------------------------------------------

-- SELECT
selectStatement :: Parser
selectStatement =
    reserved "SELECT" *> selectClause *>
    reserved "FROM" *> tableName *>
    optional (reserved "WHERE" *> whereClause) *>
    optional (reserved "ORDER" *> reserved "BY" *> orderByClause) *>
    optional (reserved "LIMIT" *> integer) *>
    optional (keyword  "ALLOW" *> keyword "FILTERING")

selectClause :: Parser
selectClause =  (keyword "COUNT" *> parens (oneOf ['*','1']))
            <|> commaSep1 selector
            <|> symbol "*"

selector :: Parser
selector =  (keyword "WRITETIME" *> parens ident)
        <|> (keyword "TTL" *> parens ident)
        <|> try (functionName *> parens (commaSep selector))
        <|> ident

whereClause :: Parser
whereClause = sepBy1 relation (reserved "AND") *> pure ()

relation :: Parser
relation =  try (ident *> relationOp *> term)
        <|> ident *> reserved "IN" *> parens (commaSep1 term)
        <|> reserved "TOKEN" *> parens (commaSep1 ident) *> relationOp *> term

relationOp :: Parser
relationOp =  reservedOp "="
          <|> try (reservedOp "<=")
          <|> try (reservedOp ">=")
          <|> reservedOp "<"
          <|> reservedOp ">"

orderByClause :: Parser
orderByClause = commaSep1 ordering

ordering :: Parser
ordering = ident *> optional (reserved "ASC" <|> reserved "DESC")

-- INSERT
insertStatement :: Parser
insertStatement =
    reserved "INSERT" *> reserved "INTO" *> tableName *>
    parens (commaSep1 ident) *> keyword "VALUES" *>
    parens (commaSep1 term) *>
    optional insertUpdateUsingClause

insertUpdateOption :: Parser
insertUpdateOption =  (keyword "TIMESTAMP" *> integer)
                  <|> (keyword "TTL" *> integer)

insertUpdateUsingClause :: Parser
insertUpdateUsingClause = reserved "USING" <* sepBy1 insertUpdateOption (reserved "AND")

-- UPDATE
updateStatement :: Parser
updateStatement = reserved "UPDATE" *> tableName *>
    optional insertUpdateUsingClause *>
    reserved "SET" *> commaSep1 assignment *>
    reserved "WHERE" *> whereClause

assignment :: Parser
assignment =  try (ident *> reservedOp "=" *> term *> optional (reservedOp "+" *> ident))
          <|> try (ident *> reservedOp "=" *> ident *>
                   lexeme (oneOf ['+','-']) *> term)
          <|> ident *> brackets term *> reservedOp "=" *> term

-- DELETE

deleteStatement :: Parser
deleteStatement =
    reserved "DELETE" *> commaSep deleteSelection *>
    reserved "FROM" *> tableName *>
    optional (reserved "USING" *> keyword "TIMESTAMP" *> integer) *>
    reserved "WHERE" *> whereClause

deleteSelection :: Parser
deleteSelection = ident *> optional (brackets term)

-- BATCH

batchStatement :: Parser
batchStatement =
    reserved "BEGIN" *>
    optional (keyword "UNLOGGED" <|> keyword "COUNTER") *>
    reserved "BATCH" *>
    optional insertUpdateUsingClause *>
    skipMany1 (stmt *> semi) *>
    reserved "APPLY" *> reserved "BATCH"
  where
    stmt = insertStatement <|> updateStatement <|> deleteStatement


-- DDL
-------------------------------------------------------------------------------

useStatement :: Parser
useStatement = reserved "USE" *> ident

createKeyspaceStatement :: Parser
createKeyspaceStatement =
    reserved "CREATE" *> reserved "KEYSPACE" *> ident *>
    reserved "WITH" *> properties

alterKeyspaceStatement :: Parser
alterKeyspaceStatement =
    reserved "ALTER" *> reserved "KEYSPACE" *> ident *>
    reserved "WITH" *> properties

dropKeyspaceStatement :: Parser
dropKeyspaceStatement = reserved "DROP" *> reserved "KEYSPACE" *> ident

createTableStatement :: Parser
createTableStatement =
    reserved "CREATE" *> (reserved "TABLE" <|> reserved "COLUMNFAMILY") *>
    tableName *> parens (commaSep1 columnDefinition) *>
    optional (reserved "WITH" *> sepBy1 tableOption (reserved "AND"))

tableOption :: Parser
tableOption =  property
           <|> keyword "COMPACT"    *> keyword "STORAGE"
           <|> keyword "CLUSTERING" *> reserved "ORDER" *> reserved "BY" *>
                  parens (commaSep1 (ident *> (reserved "ASC" <|> reserved "DESC")))

columnDefinition :: Parser
columnDefinition =  (ident *> columnType *> optional (reserved "PRIMARY" *> keyword "KEY"))
                <|> (reserved "PRIMARY"  *> keyword "KEY" *>
                    parens (partitionKey *> optional (comma *> commaSep ident)))

partitionKey :: Parser
partitionKey = ident <|> parens (commaSep1 ident)

columnType :: Parser
columnType = nativeType <|> collectionType <|> stringLiteral

nativeType :: Parser
nativeType = lexeme $
           (  string "ascii"
          <|> try (string "bigint")
          <|> try (string "blob")
          <|> string "boolean"
          <|> string "counter"
          <|> try (string "decimal")
          <|> string "double"
          <|> string "float"
          <|> string "int"
          <|> try (string "text")
          <|> try (string "timestamp")
          <|> string "timeuuid"
          <|> string "uuid"
          <|> try (string "varchar")
          <|> string "varint"
           ) *> pure ()

collectionType :: Parser
collectionType =  keyword  "LIST" *> angles nativeType
              <|> reserved "SET"  *> angles nativeType
              <|> keyword  "MAP"  *> angles (nativeType *> comma *> nativeType)

alterTableStatement :: Parser
alterTableStatement =
    reserved "ALTER" *> (reserved "TABLE" <|> reserved "COLUMNFAMILY") *>
    tableName *> instruction
  where
    instruction =  reserved "ALTER" *> ident *> keyword "TYPE" *> columnType
               <|> reserved "ADD"   *> ident *> columnType
               <|> reserved "WITH"  <* sepBy1 tableOption (reserved "AND")
               <|> keyword "RENAME" <* sepBy1 (ident *> reserved "TO" *> ident)
                                              (reserved "AND")

dropTableStatement :: Parser
dropTableStatement =
    reserved "DROP" *> (reserved "TABLE" <|> reserved "COLUMNFAMILY") *>
    tableName

truncateStatement :: Parser
truncateStatement = reserved "TRUNCATE" *> tableName

createIndexStatement :: Parser
createIndexStatement =
    reserved "CREATE" *> optional (keyword "CUSTOM") *> reserved "INDEX" *>
    optional ident *> reserved "ON" *> tableName *> parens ident *>
    optional (reserved "USING" *> stringLiteral)

dropIndexStatement :: Parser
dropIndexStatement = reserved "DROP" *> reserved "INDEX" *> ident
