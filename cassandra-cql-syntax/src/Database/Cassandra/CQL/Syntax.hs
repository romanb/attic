module Database.Cassandra.CQL.Syntax (cql, parseCQL) where

import Database.Cassandra.CQL.Syntax.Parser (parseCQL)
import Database.Cassandra.CQL.Syntax.TH (cql)
