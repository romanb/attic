module Tests.Database.Cassandra.CQL.Syntax.Parser (tests) where

import Control.Monad (forM_)
import Database.Cassandra.CQL.Syntax.Parser (parseCQL)
import Test.Framework
import Test.Framework.Providers.HUnit
import Test.HUnit hiding (Test)

tests :: [Test]
tests = [testWhitelist, testBlacklist]

testWhitelist :: Test
testWhitelist = testCase "Whitelist" $
    forM_ whitelist $ \s -> assertEqual s s (testParse s)

testBlacklist :: Test
testBlacklist = testCase "Blacklist" $
    forM_ blacklist $ \s -> assertBool s (testParse s /= s)

testParse :: String -> String
testParse s =
    case parseCQL ("", 0, 0) s of
        Left  err -> show err
        Right _   -> s

-- CQL statements that should all parse successfully
whitelist :: [String]
whitelist = [ "SELECT time, value FROM events \
               \WHERE event_type = 'myEvent' \
                    \AND time > '2011-02-03' \
                    \AND time <= '2012-01-01';"

            , "SELECT * FROM periods \
               \WHERE TOKEN(period_name) > TOKEN('Third Age') \
                    \AND TOKEN(period_name) < TOKEN('Fourth Age');"

            , "SELECT WRITETIME (first_name) FROM users \
               \WHERE last_name = 'Jones';"

            , "SELECT writetimefoo FROM users \
               \WHERE last_name = 'Jones';"

            , "SELECT TTL (first_name) FROM users \
               \WHERE last_name = 'Jones';"

            , "INSERT INTO NerdMovies (movie, director, main_actor, year) \
               \VALUES ('Serenity', 'Joss Whedon', 'Nathan Fillion', 2005) USING TTL 86400;"

            , "BEGIN BATCH \
                  \INSERT INTO users (userid, password, name) VALUES ('user2', 'ch@ngem3b', 'second user');\
                  \UPDATE users SET password = 'ps22dhds' WHERE userid = 'user3';\
                  \INSERT INTO users (userid, password) VALUES ('user4', 'ch@ngem3c');\
                  \DELETE name FROM users WHERE userid = 'user1';\
               \APPLY BATCH;"

            , "CREATE KEYSPACE Excelsior \
                \WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 3};"

            , "CREATE KEYSPACE Excalibur \
                \WITH replication = {'class': 'NetworkTopologyStrategy', 'DC1' : 1, 'DC2' : 3} \
                    \AND durable_writes = false;"

            , "CREATE TABLE users (\
                \user_name varchar PRIMARY KEY,\
                \password varchar,\
                \gender varchar,\
                \session_token varchar,\
                \state varchar,\
                \birth_year bigint\
              \);"

            , "CREATE TABLE monkeySpecies (\
                \species text PRIMARY KEY,\
                \common_name text,\
                \population varint,\
                \average_size int\
                \) WITH comment='Important biological records'\
                   \AND read_repair_chance = 1.0;\

                \CREATE TABLE timeline (\
                    \userid uuid,\
                    \posted_month int,\
                    \posted_time uuid,\
                    \body text,\
                    \posted_by text,\
                    \foobar blob,\
                    \PRIMARY KEY (userid, posted_month, posted_time)\
                \) WITH compaction = { 'class' : 'LeveledCompactionStrategy' };"

            , "ALTER TABLE addamsFamily \
                \ALTER lastKnownLocation TYPE uuid;\

                \ALTER TABLE addamsFamily ADD gravesite varchar;\

                \ALTER TABLE addamsFamily \
                \WITH comment = 'A most excellent and useful column family'\
                 \AND read_repair_chance = 0.2;"

            , "CREATE INDEX userIndex ON NerdMovies (user);\
                \CREATE INDEX ON Mutants ( abilityId );\
                \CREATE CUSTOM INDEX ON users (email) USING 'path.to.the.IndexClass';"

            , "DROP INDEX userIndex;"

            , "SELECT user_id, emails FROM users WHERE user_id = 'frodo';"

            , "SELECT * FROM playlists WHERE id = 62c36092-82a1-3a00-93d1-46196ee77204 \
                \ORDER BY song_order DESC LIMIT 50;"

            , "Select * FROM ruling_stewards \
                \WHERE king = 'none' \
                    \AND reign_start >= 1500 \
                    \AND reign_start < 3000 LIMIT 10 ALLOw FILTERING;"

            , "SELECT COUNT(*) FROM users;"

            , "SELECT Name, Occupation FROM People WHERE empID IN (199, 200, 207);"

            , "UPDATE UserActionCounts SET total = total + 2 WHERE keyalias = 523;"

            , "UPDATE excelsior.clicks USING TTL 432000 \
                  \SET user_name = 'bob' \
                  \WHERE userid=cfd66ccc-d857-4e90-b1e5-df98a3d40cd6 \
                    \AND url='http://google.com';"

            , "UPDATE Movies SET col1 = 1, col2 = 'y', col3 = ? WHERE movieID = 42;"

            , "UPDATE Movies SET col3 = 'hello' WHERE movieID IN (1, 2, 3);"

            , "UPDATE Movies SET col4 = 22 WHERE movieID =123;"

            , "UPDATE users \
                \SET state = 'TX' \
                \WHERE user_uuid \
                  \IN (88b8fd18-b1ed-4e96-bf79-4280797cba80,\
                      \06a8913c-c0d6-477c-937d-6c1b69a95d43,\
                      \bc108776-7cb5-477f-917d-869c12dfffa8);"

            , "UPDATE users \
                \SET name = 'John Smith',\
                \email = 'jsmith@cassie.com' \
                \WHERE user_uuid = 88b8fd18-b1ed-4e96-bf79-4280797cba80;"

            , "UPDATE counterks.page_view_counts \
                \SET counter_value = counter_value + 2 \
                \WHERE url_name='www.datastax.com' AND page_name='home';"

            , "UPDATE users SET emails = emails + {'fb@friendsofmordor.org'} WHERE user_id = 'frodo';"

            , "UPDATE users SET emails = emails - {'fb@friendsofmordor.org'} WHERE user_id = 'frodo';"

            , "UPDATE users SET emails = {} WHERE user_id = 'frodo';"

            , "UPDATE users \
                \SET todo = {\
                    \'2012-9-24' : 'enter mordor',\
                    \'2012-10-2 12:00' : 'throw ring into mount doom'\
                \} WHERE user_id = 'frodo';"

            , "UPDATE users SET todo = todo + {'2013-1-1': 'do stuff'} \
                \WHERE user_id='abc';"

            , "UPDATE users SET todo['2012-10-2 12:10'] = 'die' \
                \WHERE user_id = 'frodo';"

            , "UPDATE users USING TTL 1234 \
                \SET todo['2012-10-1'] = 'find water' WHERE user_id = 'frodo';"

            , "UPDATE users SET top_places = [ 'rivendell', 'rohan' ] WHERE user_id = 'frodo';"

            , "UPDATE users SET top_places = [ 'the shire' ] + top_places WHERE user_id = 'frodo';"

            , "UPDATE users SET top_places = top_places + [ 'mordor' ] WHERE user_id = 'frodo';"

            , "UPDATE users SET top_places[2] = 'riddermark' WHERE user_id = 'frodo';"

            , "UPDATE users SET top_places = top_places - ['riddermark'] WHERE user_id = 'frodo';"

            , "UPDATE users SET emails = {'a@b.com'} WHERE user_id = 'frodo';"

            , "DELETE emails FROM users WHERE user_id = 'frodo';"

            , "DELETE top_places[3] FROM users WHERE user_id = 'frodo';"

            , "ALTER TABLE users ADD todo map<timestamp, text>;"

            , "ALTER TABLE users RENAME todo TO todos"

            , "DELETE todo['2012-9-24'] FROM users WHERE user_id = 'frodo';"

            , "SELECT \"from\" FROM users WHERE user_id = 'frodo';"

            , "INSERT INTO test (id, value_float, value_double) \
                \VALUES ('test1', -2.6034345E+38, -2.6034345E+38);"

            , "SELECT * FROM myTable \
                \WHERE t > maxTimeuuid('2013-01-01 00:05+0000')\
                  \AND t < minTimeuuid('2013-02-02 10:00+0000');"

            , "SELECT * FROM myTable WHERE t = now();"

            , "CREATE TABLE song_tags (id uuid, tag_name text,PRIMARY KEY (id, tag_name));"

            , "SELECT * FROM \"CompositeUser\" WHERE \"userId\"='mevivs' LIMIT 100 ALLOW FILTERING;"

            , "CREATE TABLE example (\
                \partitionKey1 text,\
                \partitionKey2 text,\
                \clusterKey1 text,\
                \clusterKey2 text,\
                \normalField1 text,\
                \normalField2 text,\
                \PRIMARY KEY ((partitionKey1, partitionKey2), clusterKey1, clusterKey2));"

            , "INSERT INTO phonelists (user, phonenumbers) VALUES ('john',{'patricia':'555-4326','doug':'555-1579'});"

            , "INSERT INTO friendlists (user, friends) VALUES ('john',['doug','patricia','scott']);"

            , "select dateOf(id), id from ks.reports where token(id) > token(minTimeuuid('2013-07-16 16:12:48+0300'));"

            , "insert into songs (id, title, artist, femaleSinger, timesPlayed, comment) values (?, ?, ?, ?, ?, null)"
            ]

blacklist :: [String]
blacklist = [ "SELECT from FROM foo WHERE user_id=1" -- usage of reserved word
            ]

