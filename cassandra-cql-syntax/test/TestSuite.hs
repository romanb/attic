module Main where

import Test.Framework (defaultMain, testGroup)
import qualified Tests.Database.Cassandra.CQL.Syntax.Parser as Parser

main :: IO ()
main = defaultMain tests
  where
    tests =
        [ testGroup "Tests.Database.Cassandra.CQL.Syntax.Parser" Parser.tests
        ]
