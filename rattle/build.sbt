name := "rattle"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.10.1"

scalacOptions ++= Seq("-feature")

libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.0.0"

libraryDependencies += "org.typelevel" %% "scalaz-contrib-210" % "0.1.4"

libraryDependencies += "io.netty" % "netty-all" % "4.0.0.CR3"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.10.1" % "test"

libraryDependencies += "org.scalaz" %% "scalaz-scalacheck-binding" % "7.0.0" % "test"

libraryDependencies += "org.specs2" %% "specs2" % "1.12.3" % "test"

libraryDependencies += "org.typelevel" %% "scalaz-specs2" % "0.1" % "test"
