package org.pkaboo

package object rattle {
  type Params = Map[String, Vector[String]]
  type Headers = Map[String, Vector[String]]
}
