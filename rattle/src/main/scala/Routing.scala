package org.pkaboo.rattle

import scala.language.higherKinds
import scalaz.std.map._
import scalaz.std.string._
import scalaz.std.vector._
import scalaz.syntax.monoid._
import scalaz.syntax.monad._
import scala.concurrent.Future

private[rattle] object Routing {
  import Request.modifyQueryParams

  case class RouteTree[M[_], A]
    ( paths: Map[String, RouteTree[M, A]] = Map.empty[String, RouteTree[M, A]] // dirs
    , capture: Option[RouteTree[M, A]] = None
    , handler: Option[(M[A], List[String])] = None
    )
    // isFallback/CatchAll: Boolean

  private sealed trait PathSegment { def name: String } // PathSeg
  private case class ConstSegment(name: String) extends PathSegment // DirSeg
  private case class ParamSegment(name: String) extends PathSegment // CaptureSeg

  def mkTree[M[_], A](routes: List[(String, M[A])]): RouteTree[M, A] = {
    @annotation.tailrec
    def step(routes: List[(String, M[A])], tree: RouteTree[M, A]): RouteTree[M, A] =
      routes match {
        case Nil => tree
        case (p, h) :: rs => step(rs, addRoute(tree, parsePath(p), h))
      }
    step(routes, RouteTree[M, A]())
  }

  def splitPath(path: String): List[String] =
    path.split("/").filterNot(_.isEmpty).toList

  private def parsePath(path: String): List[PathSegment] =
    splitPath(path).map { s =>
      if (s startsWith ":") ParamSegment(s.tail)
      else ConstSegment(s)
    }

  def printTree[M[_], A](tree: RouteTree[M, A]): Unit = {
    def step(t: RouteTree[M, A], depth: Int): Unit = {
      println(("  " * depth) + t.handler.fold("<no-handler>")(_.toString))
      (t.paths ++ t.capture.fold(Map.empty[String, RouteTree[M, A]])(c => Map(":<capture>" -> c))).foreach {
        case (p, n) =>
          println(("  " * depth) + p)
          step(n, depth + 1)
      }
    }
    step(tree, 0)
  }

  private def addRoute[M[_], A](tree: RouteTree[M, A], path: List[PathSegment], handler: M[A]): RouteTree[M, A] = {
    def step(tree: RouteTree[M, A], path: List[PathSegment], names: List[String]): RouteTree[M, A] = {
      path match {
        case Nil => tree.copy(handler = Some(handler -> names.reverse))
        case p :: ps => p match {
          case ConstSegment(c) =>
            val s2 = tree.paths.get(c) getOrElse RouteTree[M, A]()
            tree.copy(paths = tree.paths + (c -> step(s2, ps, names)))
          case ParamSegment(p) =>
            val s2 = tree.capture.getOrElse(RouteTree[M, A]())
            tree.copy(capture = Some(step(s2, ps, p :: names)))
        }
      }
    }
    step(tree, path, Nil)
  }

  def lookup[M[_], A](tree: RouteTree[M, A])(path: String)(implicit H: MonadHandler[M]): Option[M[A]] = {
    @annotation.tailrec
    def step(path: List[String], node: RouteTree[M, A], values: Vector[String]): Option[M[A]] =
      path match {
        case Nil => node.handler.map {
          case (h, names) =>
            //val zipped: Vector[(String, String)] = (names zip values)(collection.breakOut)
            assert(values.size == names.size, s"Mismatching number of captured values. Names: ${names.size}, Values: ${values.size}")
            val params = (values zip names).groupBy(_._2).mapValues(_.map(_._1))
            H.modifyRequest(modifyQueryParams(_ |+| params)) >> h
        }
        case p :: ps => node.paths.get(p) match {
          case Some(node) => step(ps, node, values)
          case None => node.capture match {
            case Some(c) => step(ps, c, values :+ p)
            case None => None
          }
        }
      }

    step(splitPath(path), tree, Vector.empty)
  }
}
