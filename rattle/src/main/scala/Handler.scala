package org.pkaboo.rattle

import scala.language.higherKinds
import scala.concurrent.{ ExecutionContext, Future }
import scalaz.{ Monad, Show, NonEmptyList, StateT }
import scalaz.contrib.std._

sealed trait Handler[+A] {
  private[rattle] def unwrap: Handler.HandlerSt[A]

  private[rattle] final def eval(hs: HandlerState)(implicit ec: ExecutionContext): Future[Option[A]] =
    unwrap.eval(hs) map {
      case HandlerValue(a) => Some(a)
      case HandlerResponse(_) => None
    }

  private[rattle] final def exec(hs: HandlerState)(implicit ec: ExecutionContext): Future[HandlerState] =
    unwrap.exec(hs)

  private[rattle] final def run(hs: HandlerState)(implicit ec: ExecutionContext): Future[(HandlerState, HandlerResult[A])] =
    unwrap.run(hs)

  def map[B](f: A => B)(implicit ec: ExecutionContext): Handler[B] =
    Handler(unwrap map {
      case r @ HandlerResponse(_) => r
      case HandlerValue(a) => HandlerValue(f(a))
    })

  def flatMap[B](f: A => Handler[B])(implicit ec: ExecutionContext): Handler[B] =
    Handler(unwrap flatMap {
      case r @ HandlerResponse(_) => StateT(s => Future.successful((s, r)))
      case HandlerValue(a) => f(a).unwrap
    })
}

sealed trait HandlerResult[+A]
case class HandlerResponse(response: Response) extends HandlerResult[Nothing]
case class HandlerValue[A](value: A) extends HandlerResult[A]
// case class HandlerPass(msg: String) extends HandlerResult[Nothing]

case class Request
  ( method: Method
  , uri: String
  , path: String
  //, queryString: String
  , contextPath: String
  , headers: Headers
  , queryParams: Params
  // , postParams: Params
  //, pathParams: Params
  //, body: String
  ) {
  lazy val params = queryParams // ++ postParams
}

trait HasHeaders {
  // type Self
  // def setHeaders(headers: Headers): Self => Self
}

object Request extends HasHeaders {
  def modifyQueryParams(f: Params => Params): Request => Request =
    req => req.copy(queryParams = f(req.queryParams))

  // def queryParam(name: String): Request => Option[Vector[String]] =
  //  req => req.queryParams.get(name)
}

case class Response
  ( statusCode: Int
  , statusText: String
  , headers: Headers
  //, body: String
  )

object Response extends HasHeaders {
  val empty = Response(200, "OK", Map.empty)
  val notFound = Response(404, "Not Found", Map.empty)

  def setResponseCode(code: Int): Response => Response =
    _.copy(statusCode = code)

  // def setResponseBody(body: String): Response => Response =
  //   _.copy(body = body)
}

case class HttpCookie
  ( name: String
  , value: String
  , expires: Option[Long]
  , domain: Option[String]
  , path: Option[String]
  , secure: Boolean
  , httpOnly: Boolean )

case class HandlerState(request: Request, response: Response)

sealed class Method(val name: String)
object Method {
  def apply(name: String) = new Method(name)

  case object GET extends Method("GET")
  case object POST extends Method("POST")
  case object DELETE extends Method("DELETE")
  case object HEAD extends Method("HEAD")
  case object TRACE extends Method("TRACE")
  case object PUT extends Method("PUT")
  case object OPTIONS extends Method("OPTIONS")
  case object CONNECT extends Method("CONNECT")
  case object PATCH extends Method("PATCH")

  def forName(name: String): Method = name.toUpperCase match {
    case "GET" => GET
    case "POST" => POST
    case "DELETE" => DELETE
    case "HEAD" => HEAD
    case "TRACE" => TRACE
    case "PUT" => PUT
    case "OPTIONS" => OPTIONS
    case "CONNECT" => CONNECT
    case "PATCH" => PATCH
    case n => Method(n)
  }
}

// object HandlerState {
//  def gets()
// }

object Headers {
  // def addHeader[A <: HasHeaders](name: String, value: String): A => A =
  //   a => a.setHeaders(a.getHeaders |+| Map(name, Vector(value)))
}

trait MonadHandler[M[_]] extends Monad[M] {
  import Routing.{ mkTree, printTree, lookup }
  import scalaz.syntax.monad._

  private implicit def H: MonadHandler[M] = this

  private def gets[A](f: HandlerState => HandlerResult[A]): M[A] =
    liftHandler(Handler(StateT(s => Future.successful((s, f(s))))))

  private def modify(f: HandlerState => HandlerState): M[Unit] =
    liftHandler(Handler(StateT(s => Future.successful((f(s), HandlerValue(()))))))

  def liftHandler[A](sa: Handler[A]): M[A]

  def liftFuture[A](f: Future[A])(implicit ec: ExecutionContext): M[A] =
    liftHandler(Handler(StateT(s => f.map(a => (s, HandlerValue(a))))))

  def liftValue[A](value: A)(implicit ec: ExecutionContext): M[A] =
    liftFuture(Future.successful(value))

  def getRequest: M[Request] =
    gets(ss => HandlerValue(ss.request))

  def getsRequest[A](f: Request => A): M[A] =
    map(getRequest)(f)

  def getResponse: M[Response] =
    gets(ss => HandlerValue(ss.response))

  def getsResponse[A](f: Response => A): M[A] =
    map(getResponse)(f)

  def putRequest(req: Request): M[Unit] =
    modify(_.copy(request = req))

  def putResponse(resp: Response): M[Unit] =
    modify(_.copy(response = resp))

  def withRequest[A](f: Request => M[A]): M[A] =
    bind(getRequest)(f)

  def withResponse[A](f: Response => M[A]): M[A] =
    bind(getResponse)(f)

  def modifyResponse(f: Response => Response): M[Unit] =
    modify(ss => ss.copy(response = f(ss.response)))

  def modifyRequest(f: Request => Request): M[Unit] =
    modify(ss => ss.copy(request = f(ss.request)))

  def localRequest[A](f: Request => Request)(m: M[A]): M[A] =
    for {
      oldReq <- getRequest
      _      <- putRequest(f(oldReq))
      a      <- m
      _      <- putRequest(oldReq)
    } yield a

  def route[A](routes: List[(String, M[A])]): M[A] = {
    val tree = mkTree(routes)
    for {
      p <- getsRequest(_.path)
      r <- lookup(tree)(p) getOrElse finishWith(Response.notFound)
    } yield r
  }

  def routeMethods[A](methods: Map[Method, M[A]]): M[A] = {
    val methodNotAllowed = Response(405, "Method Not Allowed", Map(
      "Allow" -> Vector(methods.keys.mkString(","))
    ))
    for {
      m <- getsRequest(_.method)
      r <- methods.get(m) getOrElse finishWith(methodNotAllowed)
    } yield r
  }

  // def methods[A](methods: Method): M[A] = {
  //   val _methods = methods.toSet
  //   getsRequest(_.method).flatMap(m => if (_methods.contains(m)) )
  // }

  def finishWith[A](r: Response): M[A] =
    liftHandler(Handler(StateT(s => Future.successful((s, HandlerResponse(r))))))

  def getQueryParam(name: String): M[Option[Vector[String]]] =
    getsRequest(_.queryParams.get(name))
}

object MonadHandler {
  def apply[M[_]](implicit M: MonadHandler[M]) = M
}

object Handler {
  import scalaz.{ Kleisli, ReaderT, WriterT, Monoid, Applicative, ReaderWriterStateT }

  implicit def handlerMonadHandler(implicit ec: ExecutionContext) = new MonadHandler[Handler] {
    def point[A](a: => A): Handler[A] = Handler(StateT.stateT(HandlerValue(a)))
    def bind[A, B](fa: Handler[A])(f: A => Handler[B]): Handler[B] = fa flatMap f
    def liftHandler[A](sa: Handler[A]) = sa
  }
  implicit def readerTMonadHandler[R](implicit ec: ExecutionContext): MonadHandler[({type l[a]=ReaderT[Handler,R,a]})#l] = new MonadHandler[({type l[a]=ReaderT[Handler,R,a]})#l] {
    def liftHandler[A](sa: Handler[A]): ReaderT[Handler,R,A] = Kleisli[Handler,R,A](_ => sa)
    def bind[A, B](fa: Kleisli[Handler,R,A])(f: A => Kleisli[Handler,R,B]): Kleisli[Handler,R,B] = fa flatMap f
    def point[A](a: => A): Kleisli[Handler,R,A] = Applicative[({type l[a]=Kleisli[Handler,R,a]})#l].point(a)
  }
  implicit def writerTMonadHandler[W: Monoid](implicit ec: ExecutionContext): MonadHandler[({type l[a]=WriterT[Handler,W,a]})#l] = new MonadHandler[({type l[a]=WriterT[Handler,W,a]})#l] {
    def liftHandler[A](sa: Handler[A]): WriterT[Handler,W,A] = WriterT[Handler,W,A](sa.map((Monoid[W].zero, _)))
    def bind[A, B](fa: WriterT[Handler,W,A])(f: A => WriterT[Handler,W,B]): WriterT[Handler,W,B] = fa flatMap f
    def point[A](a: => A): WriterT[Handler,W,A] = Applicative[({type l[a]=WriterT[Handler,W,a]})#l].point(a)
  }
  implicit def stateTMonadHandler[S](implicit ec: ExecutionContext): MonadHandler[({type l[a]=StateT[Handler,S,a]})#l] = new MonadHandler[({type l[a]=StateT[Handler,S,a]})#l] {
    def liftHandler[A](sa: Handler[A]): StateT[Handler,S,A] = StateT[Handler,S,A](s => sa.map((s, _)))
    def bind[A, B](fa: StateT[Handler,S,A])(f: A => StateT[Handler,S,B]): StateT[Handler,S,B] = fa flatMap f
    def point[A](a: => A): StateT[Handler,S,A] = Applicative[({type l[a]=StateT[Handler,S,a]})#l].point(a)
  }
  implicit def rwsTMonadHandler[R, W: Monoid, S](implicit ec: ExecutionContext): MonadHandler[({type l[a]=ReaderWriterStateT[Handler,R,W,S,a]})#l] = new MonadHandler[({type l[a]=ReaderWriterStateT[Handler,R,W,S,a]})#l] {
    def liftHandler[A](sa: Handler[A]): ReaderWriterStateT[Handler,R,W,S,A] = ReaderWriterStateT[Handler,R,W,S,A]((r, s) => sa.map((Monoid[W].zero, _, s)))
    def bind[A, B](fa: ReaderWriterStateT[Handler,R,W,S,A])(f: A => ReaderWriterStateT[Handler,R,W,S,B]): ReaderWriterStateT[Handler,R,W,S,B] = fa flatMap f
    def point[A](a: => A): ReaderWriterStateT[Handler,R,W,S,A] = Applicative[({type l[a]=ReaderWriterStateT[Handler,R,W,S,a]})#l].point(a)
  }

  type HandlerSt[+A] = StateT[Future, HandlerState, HandlerResult[A]]

  private[rattle] def apply[A](s: HandlerSt[A]): Handler[A] =
    new Handler[A] { def unwrap = s }

  // def alternatives[A](handlers: NonEmptyList[Snap[Option[A]]]): Snap[A] = // chain
  //   handlers.list.reduceLeft { (h1, h2) =>
  //     h1.flatMap {
  //       case x @ Some(_) => MonadSnap[Snap].pure(x)
  //       case None => h2
  //     }
  //   }.flatMap {
  //     case Some(a) => MonadSnap[Snap].pure(a)
  //     case None => Snap(State.state(HandlerResponse(HttpResponse(404, "Not Found", Map.empty)))) /*finishWith(HttpResponse(404, "Not Found", Map.empty))*/
  //   }
}
