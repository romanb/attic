package org.pkaboo.rattle

import java.util.concurrent.RejectedExecutionException

import scala.language.higherKinds
//import scala.collection.immutable.Seq
import scala.concurrent.{ ExecutionContext, Future }
//import scalaz.{ Monad, State, Show, NonEmptyList }

import io.netty.buffer.Unpooled
import io.netty.channel.
  { ChannelHandlerContext,
    ChannelInboundMessageHandlerAdapter,
    ChannelFutureListener }
import io.netty.handler.codec.http.
  { HttpRequest => NHttpRequest,
    HttpResponse => NHttpResponse,
    _ }

// class NettyServerHandler[A](val handler: Snap[A])
class NettyServerHandler[A](val handler: Handler[A])(implicit val ec: ExecutionContext)
  extends ChannelInboundMessageHandlerAdapter[Object]
{
  override def messageReceived(ctx: ChannelHandlerContext, msg: AnyRef) {
    msg match {
      case nreq: NHttpRequest =>
        println("GOT REQUEST: " + nreq)
        import scala.collection.JavaConverters._
        val dec = new QueryStringDecoder(nreq.getUri)
        // TODO: If Content-Type: application/x-www-form-urlencoded => decode postParams
        val hs = HandlerState(
          request = Request(
            method = Method.forName(nreq.getMethod.name),
            uri = nreq.getUri,
            path = dec.path,
            contextPath = "",
            headers = Map() ++ nreq.headers.asScala.map(e => e.getKey -> Vector(e.getValue)),
            queryParams = Map() ++ dec.parameters.asScala.map(t => t._1 -> t._2.asScala.toVector)),
          response = Response.empty)

        handler.run(hs) onComplete {
          case scala.util.Success((hs2, result)) =>
            val resp = result match {
              case HandlerValue(_) => hs2.response
              case HandlerResponse(r) => r
            }
            val nresp = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.valueOf(resp.statusCode))
            //println("Keep-Alive: " + HttpHeaders.isKeepAlive(nreq))
            val cf = ctx.write(nresp)
            if (!HttpHeaders.isKeepAlive(nreq)) {
              println("SERVER CLOSING CONNECTION")
              cf.addListener(ChannelFutureListener.CLOSE)
            }

          case scala.util.Failure(t) =>
            t match {
              case e: RejectedExecutionException => ctx.write(new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.SERVICE_UNAVAILABLE)).addListener(ChannelFutureListener.CLOSE)
              case _ => ctx.write(new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR)).addListener(ChannelFutureListener.CLOSE)
            }
        }

      //case chunk: NHttpChunk => ctx.nextInboundMessageBuffer.add(msg)
      case _ => sys.error("Unexpected message type from upstream: %s".format(msg))
    }
  }
}


import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.Channel
import io.netty.channel.EventLoopGroup
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel


import io.netty.channel.ChannelInitializer
import io.netty.channel.ChannelPipeline
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.http.HttpRequestDecoder
import io.netty.handler.codec.http.{ HttpResponseEncoder, HttpObjectAggregator }

class TestAPI(implicit ec: ExecutionContext) {
  import Response._
  import scalaz.syntax.applicative._
  import scalaz.syntax.id._

  def fooHandler(implicit H: MonadHandler[Handler]): Handler[Future[Int]] = for {
    r <- H.getResponse
    _ = println("STATUS BEFORE: " + r.statusCode)
    _ <- H.modifyResponse(setResponseCode(411) andThen setResponseCode(412))
    uri <- H.getsRequest(_.uri)
    _ = println("URI: " + uri)
    req <- H.getRequest
    _ = println("REQUEST: " + req)
    i <- 1.point
  } yield Future.successful(i)


  // Example of stacking a ReaderT on top
  // It might be better to put all of the Handler API inside MonadHandler.
  // Then an instance is required to use the functions but type inference might
  // be better:
  // req <- getRequest[MyReader] // type hint on every call
  // vs
  // val myHandler = MyReader.monadHandler[Snap, Int]
  // import myHandler._
  // req <- getRequest
  import scalaz.{ MonadReader, ReaderT, Kleisli }
  type MyReader[+A] = ReaderT[Handler, Int, A]
  //val myReader = MonadReader[({type l[e,a]=ReaderT[Snap,e,a]})#l, Int]
  val myReader = Kleisli.kleisliMonadReader[Handler, Int]
  import myReader._
  val myHandler = MonadHandler[MyReader]
  import myHandler._
  def fooHandler2: MyReader[Future[Int]] = for {
    req <- getRequest
    _ = println("REQUEST: " + req)
    uri <- getsRequest(_.uri)
    _ = println("URI: " + uri)
    i <- ask
    _ = println("ENV IS: " + i)
  } yield Future.successful(i)

  //type MyReaderG[+A, M[_]] = ReaderT[M, Int, A]
  def fooHandler3[M[_]](implicit M: MonadHandler[M]): M[Future[Int]] = for {
    req <- M.getRequest
  } yield Future.successful(42)

  def fooHandler4(implicit H: MonadHandler[MyReader]): MyReader[Future[Int]] = for {
    req <- H.getRequest//[MyReader]
  } yield Future.successful(43)
}

// SimpleNettyServerInitializer
class HttpSnoopServerInitializer(implicit ec: ExecutionContext) extends ChannelInitializer[SocketChannel] {
  val api = new TestAPI()

  val handler = MonadHandler[Handler].route(List(
    "/foo" -> api.fooHandler,
    "/foo2" -> api.fooHandler2.run(42), // eg. run(cfg)
    "/foo/bar/baz" -> api.fooHandler,
    "/echo/:one" -> api.fooHandler,
    "/echo/:two/direct" -> api.fooHandler
  ))//.run(cfg) // if all handlers are eg. MyReader

  override def initChannel(ch: SocketChannel) {
    val p = ch.pipeline()
    p.addLast("decoder", new HttpRequestDecoder())
    p.addLast("aggregator", new HttpObjectAggregator(1048576))
    p.addLast("encoder", new HttpResponseEncoder())
    // Remove the following line if you don't want automatic content compression.
    //p.addLast("deflater", new HttpContentCompressor());
    p.addLast("handler", new NettyServerHandler(handler))
  }
}


// SimpleNettyServer
class HttpSnoopServer(val port: Int)(implicit ec: ExecutionContext) {
  val bossGroup = new NioEventLoopGroup()
  val workerGroup = new NioEventLoopGroup()

  def start() {
    val b = new ServerBootstrap()
    b.group(bossGroup, workerGroup)
      .channel(classOf[NioServerSocketChannel])
      .childHandler(new HttpSnoopServerInitializer())

    b.bind(port).sync()
    println("Server listening on port " + port)
  }

  def stop() {
    bossGroup.shutdownGracefully()
    workerGroup.shutdownGracefully()
  }
}

object Main {
  def main(args: Array[String]) {
    implicit val ec = scala.concurrent.ExecutionContext.Implicits.global
    val port = if (args.length > 0) args(0).toInt else 8080
    val server = new HttpSnoopServer(port)
    try {
      server.start()
      readLine("Press return to stop")
    } finally {
      server.stop()
      ec match {
        case es: java.util.concurrent.ExecutorService => es.shutdown()
        case _ =>
      }
    }
  }
}


