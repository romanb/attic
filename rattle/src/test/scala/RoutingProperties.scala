package org.pkaboo.rattle

import java.net.URLEncoder.encode

import scalaz.Applicative
import scala.concurrent.{ Await, ExecutionContext, Future }
import scala.concurrent.duration.Duration
import org.scalacheck._
import org.specs2._

object RoutingProperties extends Specification with ScalaCheck {
  import Arbitrary.arbitrary
  import Routing._

  implicit val ec = ExecutionContext.Implicits.global
  val Handler = MonadHandler[Handler]
  import Handler.{ route, getsRequest }

  // Generators and helpers
  // --------------------------------------------------------------------------

  def handler(i: Int): Handler[Int] = Applicative[Handler].pure(i)
  def handlerState(route: String): HandlerState =
    HandlerState(
      request = Request(Method.GET, "http://localhost:8080/" + route, route, "", Map.empty, Map.empty),
      response = Response(200, "OK", Map.empty))

  val genDir = Gen.alphaStr suchThat (s => !s.isEmpty && !s.startsWith(":")) map(encode(_, "UTF-8"))
  val genCapture = Gen.alphaStr suchThat (!_.isEmpty) map(s => ":" + encode(s, "UTF-8"))
  val genSegment = Gen.oneOf(genDir, genCapture)

  val genRoute: Gen[String] =
    for {
      n <- Gen.choose(1, 10)
      s <- Gen.listOfN(n, genSegment)
    } yield s.mkString("/")

  // Generates a random routing-table that can be passed to MonadHandler#route.
  // Every handler is given a unique return value within the routing table.
  val genRoutes: Gen[List[(String, Handler[Int])]] =
    for {
      n <- Gen.choose(0, 10)
      r <- Gen.listOfN(n, genRoute) suchThat { rs =>
             // Such that no ambiguous routes exist. Two routes are ambiguous if they
             // have the same non-capture segments at the same positions in the route.
             val rs2 = rs.map(r => splitPath(r).zipWithIndex.filter(!_._1.startsWith(":")))
             rs2.distinct.size == rs2.size
           }
    } yield r.zip((0 until n) map handler)

  // Generates a "filled" route based on a given route and a set of reserved values.
  // A "filled" route is a route where all capture variables are replaced with actual values.
  // The first part of the generated pair is an assoc. list with the actually replaced captures and the used values.
  def genFilledRoute(route: String, reserved: Set[String]): Gen[(List[(String, String)], String)] = {
    val segs = splitPath(route)
    for {
      values <- Gen.listOfN(segs.size, genDir) suchThat (_.forall(!reserved.contains(_)))
      zipped = segs zip values
      captures = zipped.filter(_._1.startsWith(":"))
      filled = zipped.map { case (s, v) => if (s.startsWith(":")) v else s }.mkString("/")
    } yield (captures, filled)
  }

  // Generates a pair of routes, one fully static, the other equal except for a single
  // path segment which has been replaced by a capture.
  val genRoutePair: Gen[(String, String)] =
    for {
      n <- Gen.choose(1, 10)
      dirs <- Gen.listOfN(n, genDir)
      cIdx <- Gen.choose(0, n-1)
      c <- genCapture
    } yield (dirs.mkString("/"), dirs.updated(cIdx, c).mkString("/"))


  // Tests
  // --------------------------------------------------------------------------

  def is =
    "For all routes in a routing tree, routing must pick the right handler " +
    "and capture any path parameters" ! Prop.forAllNoShrink(genRoutes) { routes =>
      val hs = handlerState("")
      val rh = route(routes)
      // Reserved names that we do not want to accidentily conflict with values chosen
      // as capture values, so as to avoid the situation where we generate a value
      // for a capture that coincidentally matches a static directory segment of another
      // route in the same tree and position, that appears before the route with the capture,
      // causing the "wrong" handler to be chosen for the purpose of this test.
      val reserved = routes.flatMap(r => splitPath(r._1)).toSet
      // For every route, check that the lookup succeeds, the right handler is chosen and,
      // if the route contains captures, that the values are captured in the handler params.
      routes.foldLeft(Prop.passed) { (acc, ph) => ph match { case (p, h) =>
        acc && {
          Prop.forAllNoShrink(genFilledRoute(p, reserved)) { case (captures, fr) =>
            val hs2 = hs.copy(request = hs.request.copy(uri = hs.request.uri + fr, path = fr))
            Await.result(rh.flatMap(x => getsRequest(_.queryParams -> x)).eval(hs2), Duration.Inf) match {
              case Some((params, result)) =>
                (captures.forall { case (c, v) =>
                  params.get(c.substring(1)).fold(false)(_.contains(v)) must_== true
                } must_== true) and
                (Some(result) must_== Await.result(h.eval(hs2), Duration.Inf))
              case None => ko("handler not found")
            }
          }
        }
      }}
    } ^
    "For any two routes that only differ by a segment being a static dir in one and a capture " +
    "in the other route, a lookup for the exact static route must always find the static route handler, " +
    "regardless of the order of the routes" ! Prop.forAll(genRoutePair) { case (dirRoute, capRoute) =>
      val dirHandler = handler(1)
      val capHandler = handler(2)
      val routes = List(dirRoute -> dirHandler, capRoute -> capHandler)
      val h1 = route(routes)
      val h2 = route(routes.reverse)
      val hs = handlerState(dirRoute)
      val dirHandlerResult = Await.result(dirHandler.eval(hs), Duration.Inf)
      (Await.result(h1.eval(hs), Duration.Inf) must_== dirHandlerResult) and
      (Await.result(h2.eval(hs), Duration.Inf) must_== dirHandlerResult)
    }
    end
}
