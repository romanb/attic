package org.pkaboo.rattle

import scalaz.{ Equal, StateT }
import scalaz.scalacheck.ScalazProperties._
import scalaz.scalacheck.ScalazArbitrary
import scala.concurrent.{ Await, Future, ExecutionContext }
import scala.concurrent.duration.Duration

import org.scalacheck.{ Arbitrary, Gen, Properties }
import org.scalacheck.Prop.forAll
import org.specs2.scalaz.Spec

object HandlerProperties extends Spec {
  val testState = HandlerState(
    request = Request(Method.GET, "/", "/", "", Map.empty, Map.empty),
    response = Response(200, "OK", Map.empty))

  implicit val ec = ExecutionContext.Implicits.global

  implicit def handlerArb1: Arbitrary[Handler[Int => Int]] = Arbitrary {
    for {
      i <- Arbitrary.arbitrary[Int]
    } yield Handler(StateT(s => Future.successful((s.copy(request = s.request.copy(uri = s.request.uri + "/" + i)), HandlerValue(_ * i)))))
  }

  implicit def handlerArb2: Arbitrary[Handler[Int]] = Arbitrary {
    for {
      i <- Arbitrary.arbitrary[Int]
    } yield Handler(StateT(s => Future.successful((s, HandlerValue(i)))))
  }

  implicit def snapEq = new Equal[Handler[Int]] {
    def equal(s1: Handler[Int], s2: Handler[Int]): Boolean =
      Await.result(s1.unwrap.run(testState), Duration.Inf) == Await.result(s2.unwrap.run(testState), Duration.Inf)
  }

  checkAll(monad.laws[Handler])
}
